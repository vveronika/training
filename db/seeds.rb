# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([ {:first_name => "Susan", :last_name => "No Name", :email => "susan@wgs.co.id"},
                          {:first_name => "John", :last_name => "PragProg" ,:email => "john@wgs.co.id"} ])

articles = Article.create([ {:title => "History", :content => "No Content", :user_id => 1 , :pages_count => 3},
                          {:title => "Hello World", :content => "Material", :user_id => 2 , :pages_count => 5} ])
