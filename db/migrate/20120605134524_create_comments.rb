class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content
      t.datetime :date_of_comment
      t.timestamps
    end
  end

 # create_table :table_name, :id => false do |t|
 #   t.integer :id, :options => 'PRIMARY KEY'
 # end
end
