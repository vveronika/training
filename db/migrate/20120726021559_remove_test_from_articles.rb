class RemoveTestFromArticles < ActiveRecord::Migration
  def up
    remove_column :articles, :test
  end

  def down
    add_column :articles, :test , :string
  end
end
