class AddPagesCountInArticle < ActiveRecord::Migration
  def up
    add_column :articles, :pages_count, :integer
  end

  def down
    remove_column :articles, :pages_count
  end
end
