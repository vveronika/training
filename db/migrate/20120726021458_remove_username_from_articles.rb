class RemoveUsernameFromArticles < ActiveRecord::Migration
  def up
    remove_column :articles, :username
  end

  def down
    add_column :articles, :username , :string
  end
end
