class AddStatusInArticles < ActiveRecord::Migration
  def up
    add_column :articles , :status, :boolean
  end

  def down
    remove_column :articles , :status
  end
end
