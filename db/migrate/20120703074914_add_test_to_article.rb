class AddTestToArticle < ActiveRecord::Migration
  def self.up
    add_column :articles, :test , :string
  end
  
  def self.down
    remove_column :articles, :test , :string
  end
end
