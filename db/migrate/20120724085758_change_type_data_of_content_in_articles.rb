class ChangeTypeDataOfContentInArticles < ActiveRecord::Migration
  def up
    change_column :articles, :content, :string
  end

  def down
    change_column :articles, :content, :text
  end
end
