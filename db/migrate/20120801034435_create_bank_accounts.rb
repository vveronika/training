class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.integer :user_id
      t.string :account_number
      t.string :bank_name
      t.timestamps
    end
  end
end
