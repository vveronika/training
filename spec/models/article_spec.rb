require 'spec_helper'

describe User do
  describe "Validations" do
    before(:each) do
      @article = Article.new
    end
    
    it "should be invalid without a title" do
      @article.attributes = valid_attributes.except(:title, :user_id)
      @article.should_not be_valid
      @article.title = "sometitle"
      @article.user_id = @user.id
      @article.should be_valid
    end


    it "should require a title" do
       setup_user
       article = Article.new({ :user_id => @user.id, :title => "", :content => "abcd" , :pages_count => 2})
       article.should_not be_valid
    end

    #
    #    it "should require a numeric pages count" do
    #      setup_user
    #      article = setup_article({:pages_count => "10"})
    #      article.should_not be_valid
    #    end
    #
    #    it "should require title with maximal 20 char" do
    #      article = setup_article({:title => "abcdefghijklmnopqrstuvwxyz"})
    #      article.should_not be_valid
    #    end

    it "returns the user of article -- show relationship between user and article" do
      setup_user
      article = setup_article({:user_id => @user.id})
      article.user.first_name = 'Udin'
    end
  end  

 
  protected
  def valid_attributes
    setup_user
    {
      :title => "my title",
      :content=> "my content",
      :pages_count => 2,
      :user_id=> @user.id
    }
  end

  def setup_user
    @user =  User.create!(:first_name => "Udin", :last_name => "Haryanto",:email => "udin@kiranatama.com", :address => "Bandung 100")
  end

  def setup_article(options = {})
    setup_user
    article = Article.new({ :user_id => @user.id,
        :title => "quire@example.com",
        :content => "abcd" , :pages_count => 2})
  end  
end
