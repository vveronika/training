describe "routing to articles" do
  it "routes /articles#show index page for article" do
    { :get => "/articles" }.should route_to(
      :controller => "articles",
      :action => "index"
    )
  end

  it "routes /articles/new#show create new article" do
    { :get => "/articles/new" }.should route_to(
      :controller => "articles",
      :action => "new"
    )
  end

end
