Factory.sequence :first_name do |n|
  "FirstNameUser_#{n}"
end

Factory.sequence :last_name do |n|
  "LastName_#{n}"
end

Factory.sequence :email do |n|
  "example_#{n}@example.com"
end


Factory.define :user do |user|
  user.first_name "Budi" #{ Factory.next(:first_name) }
  user.last_name "Cahyono"#{ Factory.next(:last_name) }
  user.address "address"
  user.email "abcd@example.com" #{ Factory.next(:email) }
end

Factory.sequence :title do |n|
  "title_#{n}"
end

Factory.define :article do |a| 
  a.title { FactoryGirl.next(:title) }
  a.content "Hello world"
  a.association :user_id, :factory => :user
  a.pages_count 10
end
