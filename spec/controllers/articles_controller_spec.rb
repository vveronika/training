require 'spec_helper'

describe ArticlesController do
  it "should use ArticlesController" do
    controller.should be_an_instance_of(ArticlesController)
  end

  describe "GET index" do   
    it "assigns all articles to @articles" do
      get :index
      assigns(:articles).should eq(Article.all)
    end
  end

  describe "POST create" do
    it "creates a new article" do
      setup_user
      expect {
        post :create, :article => {"title"=>"title_value", "content" => "content_value", "pages_count" => 3, "user_id" => @user.id}
      }.to change(Article, :count).by(1)
    end
  end
  
  describe "PUT update" do
    it "should show flash for succesfully update" do
      setup_user
      @article = Article.create! valid_attributes
      post :update, :id => @article.id
      response.should redirect_to(articles_url)
      flash[:notice].should =~ /Article was successfully updated./
    end
  end

  describe "DELETE destroy" do
    it "should decline a article" do
      setup_user
      @article = Article.create! valid_attributes
      delete :destroy, :id => @article.id
      assigns(:article).should be_true
    end

    it "should redirect to articles page" do
      setup_user
      @article = Article.create! valid_attributes
      delete :destroy, :id => @article.id
      response.should redirect_to(articles_url)
    end
  end

  
  protected

  def setup_user
    @user = User.create!(:first_name => "Udin", :last_name =>"Hermanto", :email => "udin@kiranatama.com", :address => "Bandung 100")
  end
  
  def valid_attributes
    setup_user
    {
      :title => "my title",
      :content=> "my content",
      :pages_count => 2,
      :user_id=> @user.id
    }
  end
  
#  def setup_article
#    setup_user
#    post :create, :article => {"title"=>"title_value", "content" => "content_value", "pages_count" => 3, "user_id" => @user.id}
#  end
end