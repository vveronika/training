class Comment < ActiveRecord::Base
   attr_accessible :content, :date_of_comment, :article_id, :user_id
   belongs_to :article
   belongs_to :user
end
