class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :address, :email, :password_hash, :password_salt ,:password_confirmation,:password
  attr_accessor :password
  has_many :articles
  has_many :not_nul_pages,
    :class_name => "Article" ,
    :foreign_key => "user_id",
    :conditions => "pages_count > 0"
   has_many :country_articles,
    :class_name => "Article",
    :foreign_key => "user_id",
    :conditions => "title like '%my country%'"
           
  has_many :my_country_articles, :class_name => "Article", :foreign_key => "user_id", :conditions => "title like '%AB%'"
  has_one :bank_account
  #  has_one :country
  #  has_many :comments , :dependent => :destroy
  
           
  before_save :encrypt_password
  
  validates :password, :presence => {:on => :create},
    :confirmation => true
                   
  #validates :first_name, :uniqueness => true
  #  validates :email, :presence => true, :on => :save # ==> validates on both create and update
  #  validates :email, :presence => true, :on => :create# ==> validates on create
  #  validates :email, :presence => true, :on => :update # ==> validates on update
  #  validates :email, :uniqueness => true

  #  validates_numericality_of :number_of_account, :allow_blank => true
  #  validates :bank_name, :presence => true, :if => :pay_via_transfer?
  #  validates :address, :presence => true, :if => "email.nil?"
  #  validates_format_of :email, :with =>/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create


  def pay_via_transfer?
    payment_type == "transfer"
  end

  #instance method
  def show_full_name
    "#{self.first_name} #{self.last_name}"
  end

  def show_day_name
    " #{self.created_at.strftime("%A")}"
  end
  
 
  #class method
  def self.show_full_name_class
    puts "I'm from Class method"
  end 

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end


end
