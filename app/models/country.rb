class Country < ActiveRecord::Base
  attr_accessible :code, :name
  belongs_to :user

  #validate :three_countries

  def three_countries    
    self.errors[:code] << "Code not valid !"  unless ["id","usa", "frc"].include?("#{code}")
  end

end
