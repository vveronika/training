class Article < ActiveRecord::Base
  attr_accessible :title, :content, :pages_count ,:user_id, :rating
  validates :title, :presence => true,
    :length => { :minimum => 3, :maximum => 20, },
    :format => {:with => /[a-zA-Z\s]+$/}, 
    :uniqueness => {:message => " tidak boleh sama"},
    :allow_blank => false
  #validates :title , :inclusion => { :in => %w(wgs kiranatama), :message => "%{value} must be 'wgs' or 'kiranatama'" }
  # validates :title, :exclusion => { :in => %w(www http https), :message => "%{value} is reserved." }
  
  
  #validates :pages_count, :numericality => true
  #validates :pages_count, :presence => true , :unless => "content == 'content'"

  validate :valid_content
  validates :pages_count, :presence => true, :unless => "title == 'visca'"
  validates :title, :presence => true, :on => :save # ==> validates on both create and update
  #validates :title, :presence => true, :on => :create # ==> validates on create
  #validates :title, :presence => true, :on => :update # ==> validates on update

  belongs_to :user
  belongs_to :writer,
    :class_name => "User",
    :foreign_key => "user_id"
 
  
  #deklarasi many to many relationship
  has_many :articles_has_categories
  has_many :categories , :through => :articles_has_categories
  has_many :comments , :dependent => :destroy 
  #------------------------------------
  has_many :valid_comments,
    :class_name => "Comment" ,
    :foreign_key => "article_id"
  scope :pages_count, where("pages_count > 0")
  scope :create_after_day, lambda {|time| where("created_at > ?", time) }
  scope :create_before_day, lambda {|time| where("created_at < ?", time) }
  scope :share_article, where("title like '%a%'")
  #scope :my_country, where("title like '%S%'")
  scope :create_after_time, lambda {|time| where("created_at > ?", time) }
  default_scope where("user_id is not null and title is not null")

  scope :have_user, lambda { |user| where("user_id = ?", user) }

  def valid_content
    self.errors[:content] << " is too short, brooo !!" if content.length < 3
  end

  #def self.search_by_title(params_title)
  #  Article.find_all_by_title(params_title)
  #end
  
  #Class method
  def self.hundred_char
    self.where("LENGTH(title) > 3")
  end

end
