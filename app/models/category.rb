class Category < ActiveRecord::Base
  attr_accessible :category_name

  has_many :articles_has_categories
  has_many :articles, :through => :articles_has_categories  

end
