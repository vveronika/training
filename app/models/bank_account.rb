class BankAccount < ActiveRecord::Base
   attr_accessible :bank_name, :account_number
   belongs_to :user
end
