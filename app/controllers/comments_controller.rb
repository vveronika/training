class CommentsController < ApplicationController

  def create
    @comment = Comment.new(params[:comment].merge(:user_id => User.first.id))
    respond_to do |format|
      @comment.save
      #format.html { redirect_to(article_path(@comment.article_id), :notice => 'Comment was successfully created.') }
      format.js {@comments = Article.find(params[:comment][:article_id].to_i).comments}
    end
  end
end
