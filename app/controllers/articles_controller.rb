class ArticlesController < ApplicationController
  #layout 'articles'
  before_filter :find_article, :except => [:index, :create, :new, :my_method_1, :welcome]
  before_filter :require_login, :only => [:new, :create, :edit, :update, :delete]
  
  $nama_var = "global variable"
  
  def welcome
    render :text => "This is Welcome Text"
  end
  
  def index
    @articles = Article.all
    #ap @articles
    #session['session_name'] = "sample for saving to session"

    #lazy loading
#    articles = Article.all
#    articles.each do |a|
#      puts a.user.email
#    end

    #pre loading
#    articles = Article.includes("user")
#    articles.each do |b|
#      puts b.user.email
#    end
   # render :layout => 'false
  end

  #view for new article
  def new
    @article = Article.new
  end

  #view for edit article
  def edit;end

  #create new article
  def create
    #debugger
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = 'Article was successfully created.'
      redirect_to :controller => :users, :action => :index
      #redirect_to :controller => :users, :action => :new
    else
      flash[:error] = "Article was failed created"
      render :action => :new
    end
  end

  #show article detail
  def show
    @comment = Comment.new
    @comments = @article.comments
    if @article.nil?
      flash[:notice] = 'Article was not found.'
      redirect_to articles_path
    end
  end

  # update an article
  def update
    if @article.update_attributes(params[:article])
      flash[:notice] = 'Article was successfully updated.'
      redirect_to articles_path#(:dari_update => "update")
      # redirect_to :controller => :articles, :action => :index #articles_path # :alert => "sucess"
      #redirect_to :controller => :home, :action => :index #, :alert => 'This is a flash alert'
    else
      flash[:notice] = "Article was failed updated"
      render :action => "edit"
    end
  end

  #delete an article
  def destroy
    if @article.destroy
      flash[:error] = "Article was successfully deleted"
    else
      flash[:notice] = "Article was failed deleted"
    end
    redirect_to articles_path
  end

  def my_method_1
  end
  
  def my_method_2
  end

  
  private
  def find_article
    @article = Article.find(params[:id])
  end

end
