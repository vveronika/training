require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_relation_between_article_and_comment
    user = User.create(:first_name => "visca", :last_name=>"veronika", :email=>"visca.veronika@kiranatama.com", :address => "Bandung")
    article = Article.create(:title => "new_title", :content => "new content", :user_id => user.id, :pages_count => 8)
    assert_not_nil article
    comment = Comment.create(:content => "new content", :article_id => article.id)
    assert_not_nil article.comments
    assert_equal comment.article.nil?, false
    assert_equal comment.article.class, Article
  end

end
