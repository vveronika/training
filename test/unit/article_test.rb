require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  
  def test_save_without_title
    article = Article.new(:content => 'new_content')
    assert_equal article.valid?, false
    assert_equal article.save, false
  end

  def test_save_with_complete_field
    setup_user
    article = Article.new(:title => "Testing", :content => "This is content", :user_id => @user.id, :pages_count => 2)
    assert_equal article.valid?, true
    assert_equal article.save, true
  end

  def test_relation_between_article_and_user
    user = User.create(:first_name => "visca", :last_name=>"veronika", :email=>"visca.veronika@kiranatama.com", :address => "Bandung")
    assert_not_nil user
    article = Article.create(:title => "new_title", :content => "new content", :user_id => user.id, :pages_count => 8)
    assert_not_nil user.articles
  end

  def test_find_article_with_share_title
    setup_user
    Article.create([
        {:title => "share", :content => "test posting", :pages_count => 3, :user_id => @user.id}, {:title => "test2", :content => "test posting2",
          :pages_count => 5,:user_id => users(:david).id},
      ])
    assert_not_nil Article.share_article
    assert_equal Article.share_article[0].title, "share"
  end

  def test_find_article_after_now
    setup_user
    Article.create([
        {:title => "Food", :content => "test posting", :pages_count => 3, :user_id => @user.id}, {:title => "drink", :content => "test
      posting2", :pages_count => 5,:user_id => @user.id}])
    assert_not_nil Article.create_after_now(Time.now)
  end

  private
  def setup_user
    @user =  User.create!(:first_name => "Udin", :last_name => "Amiruddin",:email => "udin@kiranatama.com", :address => "Bandung 100", :password => "123")
  end
end
