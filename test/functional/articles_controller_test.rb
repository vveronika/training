require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
 
  def setup
    @article = Article.find(:first)
  end

  def test_index
    test_new
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  def test_new
    test_new
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_create
    test_new
    assert_difference('Article.count') do
      post :create, :article => {:title => 'new title', :content => "new body", :pages_count => 8, :user_id => users(:david).id}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).title, "new title"
      assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to articles_path(assigns(:articles))
    assert_equal flash[:notice], 'Article was successfully created.'
  end

  def test_create_with_invalid_parameter
    test_new
    assert_no_difference('Article.count') do
      post :create, :article => {:title => nil, :content => nil}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Article was failed created'
  end

  def test_show
    test_new
    get :show, :id => @article.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_edit
    test_new
    get :edit, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_update
    test_new
    put :update, :id => Article.first.id,
      :article => {:title => 'updated title', :content => "updated body"}
    assert_not_nil assigns(:article)
    assert_equal assigns(:article).title, 'updated title'
    assert_response :redirect
    assert_redirected_to articles_path#(assigns(:article))
    assert_equal flash[:notice], 'Article was successfully updated.'
  end

  def test_update_with_invalid_parameter
    test_new
    put :update, :id => @article.id,
      :post => {:title => nil, :content => nil}
    assert_not_nil assigns(:article)
    flash[:notice] = "Article was failed updated"
  end

  def test_destroy
    test_new
    assert_difference('Article.count', -1) do
      delete :destroy, :id => Article.first.id
      assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to articles_path
    assert_equal flash[:error], "Article was successfully deleted"
  end

  def test_new
    login_as(users(:david).email)
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_relation_between_article_and_comment
    article = Article.create(:title => "new_title", :content => "new content", :user_id => users(:david).id, :rating => 10)
    assert_not_nil article
    comment = Comment.create(:article_id => article.id, :content => "my comment")
    assert_not_nil article.comments
    assert_equal article.comments.empty?, false
    assert_equal article.comments[0].class, Comment
  end

end